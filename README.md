# keycloak-almost-production

An almost-production-ready deployment of Keycloak using `docker-compose`.

- Uses self-signed TLS
- Uses postgresql as database
- Uses nginx as reverse proxy with SSL passthrough
- Uses "production" mode

Requirements: Docker and [`docker-compose`](https://docs.docker.com/compose/).

Run it:

```bash
docker-compose up --build
```

Test it:

```bash
curl -k -fsSL https://127.0.0.1:9443/auth/realms/master/.well-known/openid-configuration | jq . | less
curl -k -fsSL https://127.0.0.1:9443/auth/health/ready
curl -k -fsSL https://127.0.0.1:9443/auth/health/live
curl -k -fsSL https://127.0.0.1:9443/auth/metrics
```
