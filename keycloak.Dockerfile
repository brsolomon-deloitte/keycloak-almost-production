FROM registry1.dso.mil/ironbank/opensource/keycloak/keycloak:21.1.2
# FROM quay.io/keycloak/keycloak:21.1.2

# Initial admin user/pass
# https://www.keycloak.org/server/configuration#_setup_of_the_initial_admin_user
ENV KEYCLOAK_ADMIN="admin"
ENV KEYCLOAK_ADMIN_PASSWORD="changeme"

# If the server should expose health check endpoints at /health/{ready,live}. (default: false)
# https://www.keycloak.org/server/health
ENV KC_HEALTH_ENABLED="true"

# If the server should expose metrics at /metrics. (default: false)
# https://www.keycloak.org/server/configuration-metrics
ENV KC_METRICS_ENABLED="true"

# The log level of the root category or a comma-separated list of individual categories and their levels. (default: info)
# https://www.keycloak.org/server/logging
ENV KC_LOG_LEVEL="INFO,org.keycloak.events:DEBUG,org.keycloak.quarkus:DEBUG,org.keycloak.services:DEBUG,org.keycloak.services.scheduled:INFO,org.keycloak.quarkus.deployment:INFO"

# HTTPS/TLS

WORKDIR /tmp

RUN openssl req -x509 -newkey rsa:4096 -sha512 -nodes \
  -keyout /tmp/keycloak-tls.key  -days 10 -out /tmp/keycloak-tls.crt \
  -subj "/C=US/ST=PA/L=Philadelphia/O=Redact/OU=Dev/CN=server/emailAddress=admin@localhost" \
  -addext "subjectAltName=DNS:localhost,IP:127.0.0.1"

# Enables the HTTP listener. (default: false)
ENV KC_HTTP_ENABLED="true"
# The file path to a server certificate or certificate chain in PEM format. (default: null)
ENV KC_HTTPS_CERTIFICATE_FILE="/tmp/keycloak-tls.crt"
# The file path to a private key in PEM format. (default: null)
ENV KC_HTTPS_CERTIFICATE_KEY_FILE="/tmp/keycloak-tls.key"

# Disables dynamically resolving the hostname from request headers. (default: true)
ENV KC_HOSTNAME_STRICT="true"
ENV KC_HOSTNAME_STRICT_HTTPS="true"

# Set the path relative to / for serving resources. (default: /)
ENV KC_HTTP_RELATIVE_PATH="/auth"
# ENV KC_HOSTNAME_PATH="/auth"

ENV JAVA_OPTS_APPEND="-XX:+UseContainerSupport -XX:MaxRAMPercentage=50.0 -Dcom.redhat.fips=false -Dfile.encoding=UTF-8"

COPY --chown=1000:0 p1-keycloak-plugin.jar /opt/keycloak/providers/

WORKDIR /opt/keycloak
